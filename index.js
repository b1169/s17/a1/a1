let student = [];

function addStudent(name){
	student.push(name);
	console.log(`${name} was added to the student's list.`);
}

addStudent("John");

function countStudents(){
	console.log(`There is a total of ` +student.length + ` student(s) enrolled.`);
}

addStudent("Jane");
addStudent("Joe");

countStudents();

student.sort();

function printStudent(){
		student.forEach(
				function(element){
					console.log(element);
				}
		)
}

printStudent();

const uppercased = student.map(name => name.toUpperCase())

console.log(uppercased);
let filteredNames = [];
let j = 0;

function findStudent(name){

	changedName = name.toUpperCase();

	if(uppercased.includes(changedName)){

		for(let i=0; i<uppercased.length; i++){
			if(changedName != uppercased[i]){
				j++;
			}

			else{
				break;
			}
					
		}

		console.log(`${student[j]} is a student enrollee.`);
	}

	else if(changedName.length <= 1){

		filteredNames = student.filter(function(word) {
			return word[0] === changedName

		})

		for(let k=0; k<student.length; k++){

			if(changedName[k] == student[k][k]){

			console.log(`${filteredNames.join(',')} are enrollees.`);
			break;
			}

			else{
				console.log(`No student found with the name ${name}`);
				break;
			}
		}
		
	}

	else{
		console.log(`No student found with the name ${name}`);


	}
}


findStudent("jane");
findStudent("billy");
findStudent("jOhn");
findStudent("j");
findStudent("i");
findStudent("m");



/*const companies = [
{
	name: "Company One", category: "Finance", start: 1981, end: 2003
},

{
	name: "Company 2", category: "Finance", start: 1989, end: 2007
},
{
	name: "Company 3", category: "Finance", start: 1983, end: 1998
},
{
	name: "Company 4", category: "Retail", start: 2000, end: 2005
},
{
	name: "Company 5", category: "Finance", start: 1984, end: 1989
},
{
	name: "Company 6", category: "Retail", start: 1987, end: 2003
},
{
	name: "Company 7", category: "Finance", start: 1977, end: 2013
},
{
	name: "Company 8", category: "Finance", start: 1999, end: 2020
},
{
	name: "Company 9", category: "Retail", start: 1981, end: 1991
}

];

const ages = [33, 25, 100, 34, 5, 66, 12, 49, 17, 102];

/*companies.forEach(function(company){
	console.log(company.name)

})*/

/*const canDrink = ages.filter(
	function(age){
		if(age >= 21){
			return true;
		}
	}

	);*/

//better coding practice
/*const canDrink = ages.filter(age => age >=21);

console.log(canDrink);
*/
//filter retail companies

/*const retailCompanies = companies.filter(function(company){

	if(company.category === `Retail`){
		return true;
	}
}

	)

console.log(retailCompanies);

const financeCompanies = companies.filter(company => company.category === `Finance`);

console.log(financeCompanies);

const eightiesCompanies = companies.filter(company => company.start >= 1980 && company.start <1990);

console.log(eightiesCompanies);

const lastedCompanies = companies.filter(company => company.end - company.start >= 10);

console.log(lastedCompanies);

const testMap = companies.map(company => `${company.name} [${company.start} - ${company.end}]`);


console.log(testMap);

const agesSquare = ages.map(age => Math.sqrt(age));

const agetimes2 = ages.map(age => age * 2); 
const ageMap = ages
	.map(age => Math.sqrt(age))
	.map(age => age * 2);

console.log(agesSquare);
console.log(agetimes2);
console.log(ageMap);

//sort

const sortedCompanies = companies.sort(function(c1, c2){

	if (c1.start > c2.start){
		return 1;
	}

	else{

		return -1;
	}
});

console.log(sortedCompanies);

const sortedCompanies2 = companies.sort((c1,c2) => (c1.start> c2.start ? 1 : -1));

console.log(sortedCompanies2);

const sortedAges = ages.sort((a,b) => (a>b ? 1 : -1));

console.log(sortedAges);

const Names = ["Darwin", "Ralph", "Dan", "Ronnie", "Mike", "Carl", "Leon", "Chummi"];

const sortNames = Names.sort();

console.log(sortNames);*/

//reduce

/*let sum = 0

for(let k=0; k<ages.length; k++)
{
	sum += ages[k];
}

console.log(sum);*/

/*const sum = ages.reduce(function(total,age){

	return total + age;
}, 0);

console.log(sum);*/


/*const sum = ages.reduce((total, age) => total + age, 0);

console.log(sum);

const totalYears = companies.reduce(function(total,company){
	return total + (company.end - company.start);
}, 0);

console.log(totalYears);


const rangeYears = companies.reduce((total, company) => total + (company.end - company.start), 0);

console.log(rangeYears);

//combination

const combined = ages
	.map(age => age * 2)
	.filter(age => age >= 40)
	.sort((a,b) => a - b)
	.reduce((a,b) => a + b, 0);

console.log(combined);*/